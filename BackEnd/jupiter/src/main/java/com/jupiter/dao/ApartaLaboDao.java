package com.jupiter.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.jupiter.data.Connection;
import com.jupiter.model.ApartaLabo;

@SuppressWarnings({ "deprecation", "unchecked", "rawtypes" })
public class ApartaLaboDao {
	
	private static ApartaLaboDao instance;

	public static ApartaLaboDao getInstance() {
		if(instance == null) {
			instance = new ApartaLaboDao();
		}
		return instance;
	}  
	
	public ApartaLabo getApartaLaboPorId(long id)
	{
		Session session = Connection.getSession();
		ApartaLabo apartaLabo = session.get(ApartaLabo.class, id);
		session.close();
		return apartaLabo;
	}
	
	public ApartaLabo getApartaLaboPorMatriculaUsuarioPrincipal(String matriculaUsuarioPrincipal) {
		matriculaUsuarioPrincipal = matriculaUsuarioPrincipal.toUpperCase();
		
		Session session = Connection.getSession();
		Transaction tx = session.beginTransaction();
		String hql = "FROM ApartaLabo where matriculaUsuarioPrincipal = :matriculaUsuarioPrincipal";
		Query query = session.createQuery(hql);
		query.setString("matriculaUsuarioPrincipal", matriculaUsuarioPrincipal);
		List<ApartaLabo> apartaLabo = query.list();
		tx.commit();
		
		if(!apartaLabo.isEmpty())
			return apartaLabo.get(0);
		else
			return null;
	}
	
	public ApartaLabo getApartaLaboPorMatriculaAcompañante(String matriculaAcompañante) {
		matriculaAcompañante = matriculaAcompañante.toUpperCase();
		
		Session session = Connection.getSession();
		Transaction tx = session.beginTransaction();
		String hql = "FROM ApartaLabo where matriculaAcompañante1 = :matriculaAcompañante OR matriculaAcompañante2 = :matriculaAcompañante OR matriculaAcompañante3 = :matriculaAcompañante";
		Query query = session.createQuery(hql);
		query.setString("matriculaAcompañante", matriculaAcompañante);
		List<ApartaLabo> apartaLabo = query.list();
		tx.commit();
		
		if(!apartaLabo.isEmpty())
			return apartaLabo.get(0);
		else
			return null;
	}
	
	public List<ApartaLabo> getListaApartaLaboPorMatriculaUsuarioPrincipal(String matriculaUsuarioPrincipal){
		Session session = Connection.getSession();
		Transaction tx = session.beginTransaction();
		String hql = "FROM ApartaLabo where matriculaUsuarioPrincipal = :matriculaUsuarioPrincipal";
		Query query = session.createQuery(hql);
		query.setString("matriculaUsuarioPrincipal", matriculaUsuarioPrincipal);
		List<ApartaLabo> listaApartaLabo = query.list();
		tx.commit();
		
		if(!listaApartaLabo.isEmpty())
			return listaApartaLabo;
		else
			return null;
	}
	
	public List<ApartaLabo> getListaApartaLaboPorFechaDeInicio(String fechaInicio){
		Session session = Connection.getSession();
		Transaction tx = session.beginTransaction();
		String hql = "FROM ApartaLabo where fechaInicio = :fechaInicio";
		Query query = session.createQuery(hql);
		query.setString("fechaInicio", fechaInicio);
		List<ApartaLabo> listaApartaLabo = query.list();
		tx.commit();
		
		if(!listaApartaLabo.isEmpty())
			return listaApartaLabo;
		else
			return null;
	}
	
	public List<ApartaLabo> getListaApartaLaboPorFechaFinal(String fechaFinal){
		Session session = Connection.getSession();
		Transaction tx = session.beginTransaction();
		String hql = "FROM ApartaLabo where fechaFinal = :fechaFinal";
		Query query = session.createQuery(hql);
		query.setString("fechaFinal", fechaFinal);
		List<ApartaLabo> listaApartaLabo = query.list();
		tx.commit();
		
		if(!listaApartaLabo.isEmpty())
			return listaApartaLabo;
		else
			return null;
	} 
	
	public List<ApartaLabo> getListaApartaLaboPorIntegrantesTomaMedicamento(String integrantesTomaMedicamento){
		integrantesTomaMedicamento = integrantesTomaMedicamento.toUpperCase();
		
		Session session = Connection.getSession();
		Transaction tx = session.beginTransaction();
		String hql = "FROM ApartaLabo where integrantesTomaMedicamento = :integrantesTomaMedicamento";
		Query query = session.createQuery(hql);
		query.setString("integrantesTomaMedicamento", integrantesTomaMedicamento);
		List<ApartaLabo> listaApartaLabo = query.list();
		tx.commit();
		
		if(!listaApartaLabo.isEmpty())
			return listaApartaLabo;
		else
			return null;
	}
	
	public List<ApartaLabo> getListaApartaLaboPorIntegrantesConAlergias(String integratesConAlergias){
		integratesConAlergias = integratesConAlergias.toUpperCase();
		
		Session session = Connection.getSession();
		Transaction tx = session.beginTransaction();
		String hql = "FROM ApartaLabo where integratesConAlergias = :integratesConAlergias";
		Query query = session.createQuery(hql);
		query.setString("integratesConAlergias", integratesConAlergias);
		List<ApartaLabo> listaApartaLabo = query.list();
		tx.commit();
		
		if(!listaApartaLabo.isEmpty())
			return listaApartaLabo;
		else
			return null;
	} 
	
	public List<ApartaLabo> getListaApartaLaboPorEstado(String estado){
		estado = estado.toUpperCase();
		
		Session session = Connection.getSession();
		Transaction tx = session.beginTransaction();
		String hql = "FROM ApartaLabo where estado = :estado";
		Query query = session.createQuery(hql);
		query.setString("estado", estado);
		List<ApartaLabo> listaApartaLabo = query.list();
		tx.commit();
		
		if(!listaApartaLabo.isEmpty())
			return listaApartaLabo;
		else
			return null;
	} 
	
	public boolean agregaApartaLabo(ApartaLabo apartaLabo) {
		try {
			Session session = Connection.getSession();
			Transaction tx = session.beginTransaction();

			session.persist(apartaLabo);
			tx.commit();
			session.close();

			return true;
		} catch(Exception e) {
			return false;
		}
	}
	
	public boolean borrarApartaLabo(ApartaLabo apartaLabo) {
		try {
			Session session = Connection.getSession();
			Transaction tx = session.beginTransaction();

			session.delete(apartaLabo);
			tx.commit();
			session.close();

			return true;
		} catch(Exception e) {
			return false;
		}
	}
	
	public boolean actualizaApartaLabo(long id, ApartaLabo apartaLabo) {
		try {
			Session session = Connection.getSession();
			Transaction tx = session.beginTransaction();
			
			apartaLabo.setApartaLaboId(id);
			
			session.update(apartaLabo);
			tx.commit();
			session.close();

			return true;
		} catch(Exception e) {
			return false;
		}
	}
}
