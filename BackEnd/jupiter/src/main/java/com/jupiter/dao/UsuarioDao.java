package com.jupiter.dao;

import java.util.List;

import com.jupiter.data.Connection;
import com.jupiter.model.Usuario;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

@SuppressWarnings({ "deprecation", "unchecked", "rawtypes" })
public class UsuarioDao {
	
	private static UsuarioDao instance;

	public static UsuarioDao getInstance() {
		if(instance == null) {
			instance = new UsuarioDao();
		}
		return instance;
	}     
    
	
	public Usuario getUsuarioPorMatricula(String matricula)
	{
		Session session = Connection.getSession();
		Transaction tx = session.beginTransaction();
		String hql = "FROM Usuario where matricula = :matricula";
		Query query = session.createQuery(hql);
		query.setString("matricula", matricula);
		List<Usuario> users = query.list();
		tx.commit();
		
		if(!users.isEmpty())
			return users.get(0);
		else
			return null;
    }

	public List<Usuario> getListaDeUsuariosPorNombre(String nombre){
		nombre = nombre.toUpperCase();
		
		Session session = Connection.getSession();
		Transaction tx = session.beginTransaction();
		String hql = "FROM Usuario where nombre = :nombre";
		Query query = session.createQuery(hql);
		query.setString("nombre", nombre);
		List<Usuario> listaUsuarios = query.list();
		tx.commit();

		if(!listaUsuarios.isEmpty())
			return listaUsuarios;
		else
			return null;
	}
	
	public List<Usuario> getListaDeUsuariosPorApellidoPaterno(String apellidoPaterno){
		apellidoPaterno = apellidoPaterno.toUpperCase();
		
		Session session = Connection.getSession();
		Transaction tx = session.beginTransaction();
		String hql = "FROM Usuario where apellidoPaterno = :apellidoPaterno";
		Query query = session.createQuery(hql);
		query.setString("apellidoPaterno", apellidoPaterno);
		List<Usuario> listaUsuarios = query.list();
		tx.commit();
		
		if(!listaUsuarios.isEmpty())
			return listaUsuarios;
		else
			return null;
	}
	
	public List<Usuario> getListaDeUsuariosPorApellidoMaterno(String apellidoMaterno){
		apellidoMaterno = apellidoMaterno.toUpperCase();
		
		Session session = Connection.getSession();
		Transaction tx = session.beginTransaction();
		String hql = "FROM Usuario where apellidoMaterno = :apellidoMaterno";
		Query query = session.createQuery(hql);
		query.setString("apellidoMaterno", apellidoMaterno);
		List<Usuario> listaUsuarios = query.list();
		tx.commit();

		if(!listaUsuarios.isEmpty())
			return listaUsuarios;
		else
			return null;
	}
	
	public List<Usuario> getListaDeUsuariosPorRol(String rol){
		rol = rol.toUpperCase();
		
		Session session = Connection.getSession();
		Transaction tx = session.beginTransaction();
		String hql = "FROM Usuario where rol = :rol";
		Query query = session.createQuery(hql);
		query.setString("rol", rol);
		List<Usuario> listaUsuarios = query.list();
		tx.commit();

		if(!listaUsuarios.isEmpty())
			return listaUsuarios;
		else
			return null;
	}
	
	public List<Usuario> getListaDeUsuariosPorCarrera(String carrera){
		carrera = carrera.toUpperCase();
		
		Session session = Connection.getSession();
		Transaction tx = session.beginTransaction();
		String hql = "FROM Usuario where carrera = :carrera";
		Query query = session.createQuery(hql);
		query.setString("carrera", carrera);
		List<Usuario> listaUsuarios = query.list();
		tx.commit();

		if(!listaUsuarios.isEmpty())
			return listaUsuarios;
		else
			return null;
	}
	
	public List<Usuario> getListaDeUsuarios(){
		Session session = Connection.getSession();
		List<Usuario> usuarios = session.createQuery("FROM Usuario").list();
		session.close();
		
		return usuarios;
	}
	
	public boolean agregarUsuario(Usuario user) {
		try {
			Session session = Connection.getSession();
			Transaction tx = session.beginTransaction();

			session.persist(user);
			tx.commit();
			session.close();

			return true;
		}catch(Exception e){
			return false;
		}
	}
	
	public boolean borrarUsuario(String matricula) {
		try {
			Session session = Connection.getSession();
			Transaction tx = session.beginTransaction();

			Usuario user = getUsuarioPorMatricula(matricula);

			session.delete(user);
			tx.commit();
			session.close();

			return true;
		}catch(Exception e){
			return false;
		}
	}
	
	public boolean actualizarUsuario(String matricula, Usuario user) {
		try {
			Session session = Connection.getSession();
			Transaction tx = session.beginTransaction();
			
			user.setMatricula(matricula);

			session.update(user);
			tx.commit();
			session.close();
			
			return true;
		}catch(Exception e){
			return false;
		}
	}
}


