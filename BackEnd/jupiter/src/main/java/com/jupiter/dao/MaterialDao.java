package com.jupiter.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.jupiter.data.Connection;
import com.jupiter.model.Material;

@SuppressWarnings({ "deprecation", "unchecked", "rawtypes" })
public class MaterialDao {
	private static MaterialDao instance;
	
	public static MaterialDao getInstance() {
		if(instance == null) {
			instance = new MaterialDao();
		}
		return instance;
	}  
	
	public Material getMaterialPorId(String id) {
		id = id.toUpperCase();
		
		Session session = Connection.getSession();
		Transaction tx = session.beginTransaction();
		String hql = "FROM Material where id = :id";
		Query query = session.createQuery(hql);
		query.setString("id", id);
		List<Material> material = query.list();
		tx.commit();
		
		if(!material.isEmpty())
			return material.get(0);
		else
			return null;
	}
	
	public List<Material> getMaterialPorCodigo(String codigo) {
		codigo = codigo.toUpperCase();
		
		Session session = Connection.getSession();
		Transaction tx = session.beginTransaction();
		String hql = "FROM Material where codigo = :codigo";
		Query query = session.createQuery(hql);
		query.setString("codigo", codigo);
		List<Material> material = query.list();
		tx.commit();
		
		if(!material.isEmpty())
			return material;
		else
			return null;
	}

	public List<Material> getListaDeMaterialPorDescripcion(String descripcion) {
		descripcion = descripcion.toUpperCase();
		
		Session session = Connection.getSession();
		Transaction tx = session.beginTransaction();
		String hql = "FROM Material where descripcion = :descripcion";
		Query query = session.createQuery(hql);
		query.setString("descripcion", descripcion);
		List<Material> material = query.list();
		tx.commit();
		
		if(!material.isEmpty())
			return material;
		else
			return null;
	}
	
	public List<Material> getListaDeMaterialPorMarca(String marca) {
		marca = marca.toUpperCase();
		
		Session session = Connection.getSession();
		Transaction tx = session.beginTransaction();
		String hql = "FROM Material where marca = :marca";
		Query query = session.createQuery(hql);
		query.setString("marca", marca);
		List<Material> material = query.list();
		tx.commit();
		
		if(!material.isEmpty())
			return material;
		else
			return null;
	}
	
	public List<Material> getListaDeMaterialPorModelo(String modelo) {
		modelo = modelo.toUpperCase();
		
		Session session = Connection.getSession();
		Transaction tx = session.beginTransaction();
		String hql = "FROM Material where modelo = :modelo";
		Query query = session.createQuery(hql);
		query.setString("modelo", modelo);
		List<Material> material = query.list();
		tx.commit();
		
		if(!material.isEmpty())
			return material;
		else
			return null;
	}

	public List<Material> getListaDeMaterialPorEstadoFisico(String estadoFisico) {
		estadoFisico = estadoFisico.toUpperCase();
		
		Session session = Connection.getSession();
		Transaction tx = session.beginTransaction();
		String hql = "FROM Material where estadoFisico = :estadoFisico";
		Query query = session.createQuery(hql);
		query.setString("estadoFisico", estadoFisico);
		List<Material> material = query.list();
		tx.commit();
		
		if(!material.isEmpty())
			return material;
		else
			return null;
	}
	
	public List<Material> getListaDeMaterialPorUbicacion(String ubicacion) {
		ubicacion = ubicacion.toUpperCase();
		
		Session session = Connection.getSession();
		Transaction tx = session.beginTransaction();
		String hql = "FROM Material where ubicacion = :ubicacion";
		Query query = session.createQuery(hql);
		query.setString("ubicacion", ubicacion);
		List<Material> material = query.list();
		tx.commit();
		
		if(!material.isEmpty())
			return material;
		else
			return null;
	}
	
	public List<Material> getListaDeMaterialPorLaboratorio(int laboratorio) {		
		Session session = Connection.getSession();
		Transaction tx = session.beginTransaction();
		String hql = "FROM Material where laboratorio = :laboratorio";
		Query query = session.createQuery(hql);
		query.setInteger("laboratorio", laboratorio);
		List<Material> material = query.list();
		tx.commit();
		
		if(!material.isEmpty())
			return material;
		else
			return null;
	}
	
	public List<Material> getListaDeMaterialPorMatriculaUsuario(String matriculaUsuario) {
		matriculaUsuario = matriculaUsuario.toUpperCase();
		
		Session session = Connection.getSession();
		Transaction tx = session.beginTransaction();
		String hql = "FROM Material where matriculaUsuario = :matriculaUsuario";
		Query query = session.createQuery(hql);
		query.setString("matriculaUsuario", matriculaUsuario);
		List<Material> material = query.list();
		tx.commit();
		
		if(!material.isEmpty())
			return material;
		else
			return null;
	}
		
	public List<Material> getListaDeMaterial(){
		Session session = Connection.getSession();
		List<Material> material = session.createQuery("FROM Material").list();
		session.close();
		
		return material;
	}
	
	public boolean agregarMaterial(Material material) {
		try {
			Session session = Connection.getSession();
			Transaction tx = session.beginTransaction();

			session.persist(material);
			tx.commit();
			session.close();
			return true;
		}catch(Exception e) {
			return false;
		}
	}
	
	public boolean borrarMaterial(String codigo) { //Borra el ultimo material con ese codigo agregado a la BD
		try {
			Session session = Connection.getSession();
			Transaction tx = session.beginTransaction();

			List<Material> listaDeMaterial = getMaterialPorCodigo(codigo);
			
			Material ultimoMaterial = listaDeMaterial.get(listaDeMaterial.size()-1);
			
			session.delete(ultimoMaterial);
			tx.commit();
			session.close();

			return true;
		}catch(Exception e){
			return false;
		}
	}
	
	public boolean actualizaMaterial(String id, Material material) {
		try {
			Session session = Connection.getSession();
			Transaction tx = session.beginTransaction();
			
			material.setId(id);

			session.update(material);
			tx.commit();
			session.close();
			
			return true;
		}catch(Exception e){
			return false;
		}
	}
}
