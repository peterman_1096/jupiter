package com.jupiter.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.jupiter.data.Connection;
import com.jupiter.model.Prestamo;

@SuppressWarnings({ "deprecation", "unchecked", "rawtypes" })
public class PrestamoDao {

	private static PrestamoDao instance;

	public static PrestamoDao getInstance() {
		if(instance == null) {
			instance = new PrestamoDao();
		}
		return instance;
	} 
	
	public Prestamo getPrestamoPorId(long idPrestamo) {
		Session session = Connection.getSession();
		Prestamo prestamo = session.get(Prestamo.class, idPrestamo);
		session.close();

		return prestamo;
	}
	
	public List<Prestamo> getListaDePrestamosPorMatriculaUsuario(String matriculaUsuario) {
		matriculaUsuario = matriculaUsuario.toUpperCase();
		
		Session session = Connection.getSession();
		Transaction tx = session.beginTransaction();
		String hql = "FROM Prestamo where matriculaUsuario = :matriculaUsuario";
		Query query = session.createQuery(hql);
		query.setString("matriculaUsuario", matriculaUsuario);
		List<Prestamo> prestamos = query.list();
		tx.commit();
		
		if(!prestamos.isEmpty())
		{
			return prestamos;
		}
		else
			return null;
	}
	
	public List<Prestamo> getListaDePrestamosPorFechaDePrestamo(String fechaPrestamo) {
		Session session = Connection.getSession();
		Transaction tx = session.beginTransaction();
		String hql = "FROM Prestamo where fechaPrestamo = :fechaPrestamo";
		Query query = session.createQuery(hql);
		query.setString("fechaPrestamo", fechaPrestamo);
		List<Prestamo> prestamos = query.list();
		tx.commit();
		
		if(!prestamos.isEmpty())
		{
			return prestamos;
		}
		else
			return null;
	}
	
	public List<Prestamo> getListaDePrestamosPorFechaDeDevolucion(String fechaDevoluacion) {
		Session session = Connection.getSession();
		Transaction tx = session.beginTransaction();
		String hql = "FROM Prestamo where fechaDevoluacion = :fechaDevoluacion";
		Query query = session.createQuery(hql);
		query.setString("fechaDevoluacion", fechaDevoluacion);
		List<Prestamo> prestamos = query.list();
		tx.commit();
		
		if(!prestamos.isEmpty())
		{
			return prestamos;
		}
		else
			return null;
	}
	
	public List<Prestamo> getListaDePrestamosPorPrestador(String matriculaPrestador) {
		matriculaPrestador = matriculaPrestador.toUpperCase();
		
		Session session = Connection.getSession();
		Transaction tx = session.beginTransaction();
		String hql = "FROM Prestamo where matriculaPrestador = :matriculaPrestador";
		Query query = session.createQuery(hql);
		query.setString("matriculaPrestador", matriculaPrestador);
		List<Prestamo> prestamos = query.list();
		tx.commit();
		
		if(!prestamos.isEmpty())
		{
			return prestamos;
		}
		else
			return null;
	}
	
	public List<Prestamo> getListaDePrestamosPorReceptor(String matriculaReceptor) {
		matriculaReceptor = matriculaReceptor.toUpperCase();
		
		Session session = Connection.getSession();
		Transaction tx = session.beginTransaction();
		String hql = "FROM Prestamo where matriculaReceptor = :matriculaReceptor";
		Query query = session.createQuery(hql);
		query.setString("matriculaReceptor", matriculaReceptor);
		List<Prestamo> prestamos = query.list();
		tx.commit();
		
		if(!prestamos.isEmpty())
		{
			return prestamos;
		}
		else
			return null;
	}
	 
	public List<Prestamo> getPrestamoPorCodigoMaterialPrestado(String codigoMaterial) {
		codigoMaterial = codigoMaterial.toUpperCase();
		
		Session session = Connection.getSession();
		Transaction tx = session.beginTransaction();
		String hql = "FROM Prestamo where codigoMaterial = :codigoMaterial";
		Query query = session.createQuery(hql);
		query.setString("codigoMaterial", codigoMaterial);
		List<Prestamo> prestamos = query.list();
		tx.commit();
		
		if(!prestamos.isEmpty())
		{
			return prestamos;
		}
		else
			return null;
	}
	 
	public List<Prestamo> getPrestamoPorIdMaterialPrestado(String idMaterial) {
		idMaterial = idMaterial.toUpperCase();
		
		Session session = Connection.getSession();
		Transaction tx = session.beginTransaction();
		String hql = "FROM Prestamo where idMaterial = :idMaterial";
		Query query = session.createQuery(hql);
		query.setString("idMaterial", idMaterial);
		List<Prestamo> prestamos = query.list();
		tx.commit();
		
		if(!prestamos.isEmpty())
		{
			return prestamos;
		}
		else
			return null;
	}
	
	
	public Prestamo getPrestamoPorMatriculaIdMaterialEstado(String matriculaUsuario, String idMaterial, String estado) {
		matriculaUsuario = matriculaUsuario.toUpperCase();
		idMaterial = idMaterial.toUpperCase();
		estado = estado.toUpperCase();
		
		Session session = Connection.getSession();
		Transaction tx = session.beginTransaction();
		String hql = "FROM Prestamo where matriculaUsuario = :matriculaUsuario AND idMaterial =:idMaterial AND estado =:estado";
		Query query = session.createQuery(hql);
		query.setString("matriculaUsuario", matriculaUsuario);
		query.setString("idMaterial", idMaterial);
		query.setString("estado", estado);
		List<Prestamo> prestamos = query.list();
		tx.commit();
		
		if(!prestamos.isEmpty())
		{
			return prestamos.get(0);
		}
		else
			return null;
	}
	
	public List<Prestamo> getListaDePrestamosPorEstado(String estado) {
		estado = estado.toUpperCase();
		
		Session session = Connection.getSession();
		Transaction tx = session.beginTransaction();
		String hql = "FROM Prestamo where estado = :estado";
		Query query = session.createQuery(hql);
		query.setString("estado", estado);
		List<Prestamo> prestamos = query.list();
		tx.commit();
		
		if(!prestamos.isEmpty())
		{
			return prestamos;
		}
		else
			return null;
	}
	
	public boolean agregaPrestamo(Prestamo prestamo) {
		try {
			Session session = Connection.getSession();
			Transaction tx = session.beginTransaction();
	
			session.persist(prestamo);
			tx.commit();
			session.close();
	
			return true;
		}catch(Exception e){
			return false;
		}
	}
	
	public boolean actualizaPrestamo(long id, Prestamo prestamo) {
		try {
			Session session = Connection.getSession();
			Transaction tx = session.beginTransaction();
			
			prestamo.setIdPrestamo(id);
			
			session.update(prestamo);
			
			tx.commit();
			session.close();

			return true;
		}catch(Exception e){
			return false;
		}
	}
		
	public boolean BorrarPrestamo(Prestamo prestamo) {
		try {
			Session session = Connection.getSession();
			Transaction tx = session.beginTransaction();
						
			session.delete(prestamo);
			
			tx.commit();
			session.close();

			return true;
		}catch(Exception e){
			return false;
		}
	}
	
	public boolean BorrarPrestamo(long idPrestamo) {
		try {
			Session session = Connection.getSession();
			Transaction tx = session.beginTransaction();
			
			Prestamo prestamo = getPrestamoPorId(idPrestamo);
			
			session.delete(prestamo);
			
			tx.commit();
			session.close();

			return true;
		}catch(Exception e){
			return false;
		}
	}	
}
