package com.jupiter.data;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration; 


public class Connection 
{
    private static Connection instance; 
    private SessionFactory sessionFactory;
    
    public static Connection getInstance()
    {
    	if(null == instance)
    	{
    		instance=new Connection();
    	}
    	
        return instance;
    }
    
    private Connection()
    {
        Configuration configuration = new Configuration();
        configuration.configure("hibernate.login.cfg.xml");    
        sessionFactory = configuration.buildSessionFactory();   
    }
    
    public static Session getSession()
    { 
        Session session = getInstance().sessionFactory.openSession();
        return session;
    }
    
}
