package com.jupiter.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name="ApartaCajones")
public class AparataCajones {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long idPrestamo;
	@Column
	private String matriculaUsuario;
	@Column
	private boolean graduando;
	@Column
	private String tipoDeUso;	//Individual - Grupal
	@Column
	private String matriculaAcompañante1;
	@Column
	private String matriculaAcompañante2;
	@Column
	private String matriculaAcompañante3;
	
	public String getMatriculaUsuario() {
		return matriculaUsuario;
	}
	public void setMatriculaUsuario(String matriculaUsuario) {
		this.matriculaUsuario = matriculaUsuario;
	}
	public boolean isGraduando() {
		return graduando;
	}
	public void setGraduando(boolean graduando) {
		this.graduando = graduando;
	}
	public String getTipoDeUso() {
		return tipoDeUso;
	}
	public void setTipoDeUso(String tipoDeUso) {
		this.tipoDeUso = tipoDeUso;
	}
	public String getMatriculaAcompañante1() {
		return matriculaAcompañante1;
	}
	public void setMatriculaAcompañante1(String matriculaAcompañante1) {
		this.matriculaAcompañante1 = matriculaAcompañante1;
	}
	public String getMatriculaAcompañante2() {
		return matriculaAcompañante2;
	}
	public void setMatriculaAcompañante2(String matriculaAcompañante2) {
		this.matriculaAcompañante2 = matriculaAcompañante2;
	}
	public String getMatriculaAcompañante3() {
		return matriculaAcompañante3;
	}
	public void setMatriculaAcompañante3(String matriculaAcompañante3) {
		this.matriculaAcompañante3 = matriculaAcompañante3;
	}
	
}
