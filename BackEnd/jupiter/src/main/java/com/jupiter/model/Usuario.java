package com.jupiter.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name="Usuario")
public class Usuario 
{
	@Id
    private String matricula;
    @Column
	private String contrasenia;
    @Column
    private String nombre;
    @Column
    private String apellidoPaterno;
    @Column
    private String apellidoMaterno;
    @Column
    private String telefono;
    @Column
    private String email;
    @Column
    private long prestamos;
    @Column
    private String carrera;
    @Column
    private String rol;
    
    public void setMatricula(String matricula) {
    	this.matricula = matricula;
    }
    public String getMatricula() {
    	return this.matricula;
    }
    
    public String getContrasenia() {
		return contrasenia;
	}
	public void setContrasenia(String contrasenia) {
		this.contrasenia = contrasenia;
	}
	
	public void setNombre(String nombre) {
    	this.nombre = nombre;
    }
    public String getNombre() {
    	return this.nombre;
    }
    
    public void setApellidoPaterno(String apellidoPaterno) {
    	this.apellidoPaterno = apellidoPaterno;
    }
    public String getApellidoPaterno() {
    	return this.apellidoPaterno;
    }
    
    public void setApellidoMaterno(String apellidoMaterno) {
    	this.apellidoMaterno = apellidoMaterno;
    }
    public String getApellidoMaterno() {
    	return this.apellidoMaterno;    	
    }
    
    public void setTelefono(String telefono) {
    	this.telefono = telefono;
    }
    public String getTelefono() {
    	return this.telefono;
    }
  
    public void setEmail(String email) {
    	this.email = email;
    }
    public String getEmail() {
    	return this.email;
    }
    
    public void setPrestamos(long prestamos) {
    	this.prestamos = prestamos;
    }
    public long getPrestamos() {
    	return this.prestamos;
    }
    
    public void setCarrera(String carrera) {
    	this.carrera = carrera;
    }
    public String getCarrera() {
    	return this.carrera;
    }
     
	public String getRol() {
		return rol;
	}
	public void setRol(String rol) {
		this.rol = rol;
	}    
}