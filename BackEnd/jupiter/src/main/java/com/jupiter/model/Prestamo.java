package com.jupiter.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name="Prestamo")
public class Prestamo {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long idPrestamo;
	@Column
	private String matriculaUsuario;
	@Column
	private String fechaPrestamo;
	@Column
	private String matriculaPrestador;
	@Column
	private String fechaDevoluacion;
	@Column
	private String matriculaReceptor;
	@Column
	private String idMaterial;
	@Column
	private String codigoMaterial;
	@Column
	private String estado; //Activo/Finalizado
	
	
	public long getIdPrestamo() {
		return idPrestamo;
	}
	public void setIdPrestamo(long idPrestamo) {
		this.idPrestamo = idPrestamo;
	}
	
	public String getMatriculaUsuario() {
		return matriculaUsuario;
	}
	public void setMatriculaUsuario(String matriculaUsuario) {
		this.matriculaUsuario = matriculaUsuario;
	}
	
	public String getFechaPrestamo() {
		return fechaPrestamo;
	}
	public void setFechaPrestamo(String fechaPrestamo) {
		this.fechaPrestamo = fechaPrestamo;
	}
	
	public String getMatriculaPrestador() {
		return matriculaPrestador;
	}
	public void setMatriculaPrestador(String matriculaPrestador) {
		this.matriculaPrestador = matriculaPrestador;
	}
	
	public String getFechaDevoluacion() {
		return fechaDevoluacion;
	}
	public void setFechaDevoluacion(String fechaDevoluacion) {
		this.fechaDevoluacion = fechaDevoluacion;
	}
	
	public String getMatriculaReceptor() {
		return matriculaReceptor;
	}
	public void setMatriculaReceptor(String matriculaReceptor) {
		this.matriculaReceptor = matriculaReceptor;
	}
	
	public String getIdMaterial() {
		return idMaterial;
	}
	public void setIdMaterial(String idMaterial) {
		this.idMaterial = idMaterial;
	}
	
	public String getCodigoMaterial() {
		return codigoMaterial;
	}
	public void setCodigoMaterial(String codigoMaterial) {
		this.codigoMaterial = codigoMaterial;
	}
	
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
}
