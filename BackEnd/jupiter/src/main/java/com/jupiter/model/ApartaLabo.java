package com.jupiter.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name="ApartaLabo")
public class ApartaLabo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long apartaLaboId;
	@Column
	private String matriculaUsuarioPrincipal;
	@Column
	private String matriculaAcompañante1;
	@Column
	private String matriculaAcompañante2;
	@Column
	private String matriculaAcompañante3;
	@Column
	private String observaciones;
	@Column
	private String fechaInicio;
	@Column
	private String horaInicio;
	@Column
	private String fechaFinal;
	@Column
	private String horaFinal;
	@Column
	private String integrantesTomaMedicamento;	//Si - No
	@Column
	private String medicamento;
	@Column
	private String integratesConAlergias; //Si - No
	@Column
	private String alergias;
	@Column
	private String situacionMedica;
	@Column
	private String estado;
	
	public long getApartaLaboId() {
		return apartaLaboId;
	}
	public void setApartaLaboId(long apartaLaboId) {
		this.apartaLaboId = apartaLaboId;
	}
	
	public String getMatriculaUsuarioPrincipal() {
		return matriculaUsuarioPrincipal;
	}
	public void setMatriculaUsuarioPrincipal(String matriculaUsuarioPrincipal) {
		this.matriculaUsuarioPrincipal = matriculaUsuarioPrincipal;
	}
	
	public String getMatriculaAcompañante1() {
		return matriculaAcompañante1;
	}
	public void setMatriculaAcompañante1(String matriculaAcompañante1) {
		this.matriculaAcompañante1 = matriculaAcompañante1;
	}
	
	public String getMatriculaAcompañante2() {
		return matriculaAcompañante2;
	}
	public void setMatriculaAcompañante2(String matriculaAcompañante2) {
		this.matriculaAcompañante2 = matriculaAcompañante2;
	}
	
	public String getMatriculaAcompañante3() {
		return matriculaAcompañante3;
	}
	public void setMatriculaAcompañante3(String matriculaAcompañante3) {
		this.matriculaAcompañante3 = matriculaAcompañante3;
	}
	
	public String getObservaciones() {
		return observaciones;
	}
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	
	public String getFechaInicio() {
		return fechaInicio;
	}
	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	
	public String getHoraInicio() {
		return horaInicio;
	}
	public void setHoraInicio(String horaInicio) {
		this.horaInicio = horaInicio;
	}
	
	public String getFechaFinal() {
		return fechaFinal;
	}
	public void setFechaFinal(String fechaFinal) {
		this.fechaFinal = fechaFinal;
	}
	
	public String getHoraFinal() {
		return horaFinal;
	}
	public void setHoraFinal(String horaFinal) {
		this.horaFinal = horaFinal;
	}
	public String isIntegrantesTomaMedicamento() {
		return integrantesTomaMedicamento;
	}
	public void setIntegrantesTomaMedicamento(String integrantesTomaMedicamento) {
		this.integrantesTomaMedicamento = integrantesTomaMedicamento;
	}
	public String getMedicamento() {
		return medicamento;
	}
	public void setMedicamento(String medicamento) {
		this.medicamento = medicamento;
	}
	public String isIntegratesConAlergias() {
		return integratesConAlergias;
	}
	public void setIntegratesConAlergias(String integratesConAlergias) {
		this.integratesConAlergias = integratesConAlergias;
	}
	public String getAlergias() {
		return alergias;
	}
	public void setAlergias(String alergias) {
		this.alergias = alergias;
	}
	public String getSituacionMedica() {
		return situacionMedica;
	}
	public void setSituacionMedica(String situacionMedica) {
		this.situacionMedica = situacionMedica;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	
	
}
