CREATE TABLE `smpicnic`.`User` (
  `matricula` VARCHAR(10) NOT NULL,
  `apellidoPaterno` VARCHAR(45) NOT NULL,
  `apellidoMaterno` VARCHAR(45) NOT NULL,
  `Nombre` VARCHAR(45) NOT NULL,
  `celular` VARCHAR(45) NOT NULL,
  `emailPersonal` VARCHAR(45) NOT NULL,
  `prestamos` MEDIUMINT(3) UNSIGNED NULL DEFAULT 0,
  PRIMARY KEY (`matricula`),
  UNIQUE INDEX `matricula_UNIQUE` (`matricula` ASC))
COMMENT = 'Esta tabla describe al usuario que estara registrado en la plataforma Jupiter';

CREATE TABLE `Labo`.`Prestamos` (
  `idPrestamo` INT UNSIGNED NOT NULL,
  `matriculaUsuario` VARCHAR(10) NOT NULL,
  `nombreUsuario` VARCHAR(40) NOT NULL,
  `fechaPrestado` DATETIME NOT NULL,
  `prestador` VARCHAR(40) NOT NULL,
  `fechaDevuelto` DATETIME NULL,
  `receptor` VARCHAR(40) NULL,
  PRIMARY KEY (`idPrestamo`))
COMMENT = 'En esta tabla se guarda el historial de todos los prestamos realizados dentro de la plataforma';
